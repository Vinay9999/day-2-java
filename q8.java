import java.util.Scanner; 

public class q8 { 
public static void main(String[] args) { 
// TODO Auto-generated method stub 
Scanner sc=new Scanner(System.in); 
System.out.print("Enter the first number: "); 
int number1=sc.nextInt(); 
System.out.print("Enter the second number: "); 
int number2=sc.nextInt(); 
sc.close(); 
System.out.println("Reverse of first number is: "+reverse(number1)); 
System.out.println("Reverse of second number is: "+reverse(number2)); 
} 
public static int reverse(int number)
{ 
int reverse_number=0; 
while(number!=0) 
{ 
reverse_number*=10; 
reverse_number+=number%10; 
number/=10; 
} 
return reverse_number; 
} 
} 