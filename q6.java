import java.util.Scanner; 

public class q6 { 
public static void main(String[] args) { 
// TODO Auto-generated method stub 
Scanner sc=new Scanner(System.in); 
try 
{ 
System.out.print("Enter the number: "); 
int number=sc.nextInt(); 
sc.close(); 
long reverse_number=0; 
while(number!=0) 
{ 
reverse_number*=10; 
reverse_number+=number%10; 
number/=10; 
if(reverse_number<-2147483648 || reverse_number>2147483647) 
{ 
throw new Exception(); 
} 
} 
System.out.println("Reverse number is: "+(reverse_number)); 
} 
catch(Exception e) 
{ 
System.out.print("Reverse number is: "+0); 
} 
} 
} 